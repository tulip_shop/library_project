<!DOCTYPE html>
<head>
	<title>Sign in</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="./Resources/User/css/util.css">
	<link rel="stylesheet" type="text/css" href="./Resources/User/css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="bg-contact100" style="background-image: url('./Resources/User/assets/img/lib1.jpg');">
		<div class="container-contact100">
			<div class="wrap-contact100">
				<div class="contact100-pic js-tilt">
					<img src="./Resources/User/assets/img/books.png" alt="IMG">
				</div>

				<form class="contact100-form" method="post">
                    <div style="height :50px"></div>
					<span class="contact100-form-title">
						Sign in
					</span>
                    <?php include("./Code/validation.php"); ?>
					<div class="wrap-input100">
						<input class="input100" type="text" name="username" placeholder="User Name" required>
						<span><?php echo $userErr?></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 ">
						<input class="input100" type="password" name="password" placeholder="Password" required>
						<span><?php echo $passErr?></span>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="container-contact100-form-btn">
						<input type="submit" name="login" class="contact100-form-btn" value="Submit">
                        <a href="signup.php" style="margin-top : 15px;">Create Account</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
