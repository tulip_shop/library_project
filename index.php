<?php
include('Code/connection.php');
include('Code/UserFunctions.php');
session_destroy();
extract($_POST);
$c_result = sel_category($conn);
$a_result = sel_author($conn);
$result = BooksAll($conn);
if (isset($search)) {
    if ($category !== "" || $author !== "") {
        if ($category !== "" && $author !== "") {
            $result = BooksCat_Aut($category, $author, $conn);
        } elseif ($category !== "" && $author == "") {
            $result = BooksCat($category, $conn, $result);
        } else {
            $result =  BooksAut($author, $conn, $result);
        }
    }
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Library</title>
    <!-- Css Styles -->
    <link rel="stylesheet" href="Resources/User/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="Resources/User/css/style.css" type="text/css">
</head>
<body>
      <!-- Header Section Begin -->
      <div class="menu-item">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo">
                            <a href="home.php">
                            <h3>e-Book</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="nav-menu">
                            <nav class="mainmenu">
                                <ul>
                                    <li class="active"><a href="home.php">Home</a></li>
                                    <li><a href="signin.php">Sign in</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Header End -->
<section class="hero-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="hero-text" >
                        <h1>Never Stop Reading</h1>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 offset-xl-2 offset-lg-1">
                    <div class="booking-form">
                        <h3>Search Book</h3>
                        <form method="post">
                            <div class="select-option">
                                <label for="cate">Category : </label>
                                <select id="cate" name="category">
                                    <option value="" hidden></option>
                                    <?php foreach ($c_result as $val) { ?>
                                        <option value="<?php echo $val['id']; ?>"><?php echo $val['c_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select-option" name="author">
                                <label for="author">Author : </label>
                                <select id="author" name="author">
                                    <option value="" hidden></option>
                                    <?php foreach ($a_result as $val) { ?>
                                        <option value="<?php echo $val['id']; ?>"><?php echo $val['a_name'] ?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                            <input type="submit" name="search" value="Search" class="btn btn-default col-lg-12" style="border-color:#dfa974; color:#dfa974;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-slider owl-carousel">
         
            <div class="hs-item set-bg" data-setbg="Resources/User/assets/img/li.jpg"></div>
            <div class="hs-item set-bg" data-setbg="Resources/User/assets/img/lib1.jpg"></div>
            <div class="hs-item set-bg" data-setbg="Resources/User/assets/img/lib2.jpg"></div>
            <div class="hs-item set-bg" data-setbg="Resources/User/assets/img/books.jpg"></div>
        </div>
    </section>

    <div class="hp-room-items" style="margin-top : 50px;">
        <div class="row">
        <?php foreach ($result as $value) {?>
            <div class="col-lg-3 col-md-6">
                <div class="hp-room-item set-bg" data-setbg="Resources/images/<?php echo $value['image']?>" style="width: 350px; height:600px;">

                    <div class="hr-text" style="background: rgba(16, 13, 27, 0.9); padding : 20px;">
                        <h3><?php echo $value['title']; ?></h3>
                        <h2><?php echo $value['price'];?>$</h2>
                        <table>
                            <tbody>
                                <tr>
                                    <td class="r-o">Category:</td>
                                    <td><?php echo $value['c_name']; ?></td>
                                </tr>
                                <tr>
                                    <td class="r-o">Author:</td>
                                    <td><?php echo $value['a_name']; ?></td>
                                </tr>
                                <tr>
                                    <td class="r-o">Quantity:</td>
                                    <td><?php echo $value['quantity']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div><?php } ?>
        </div>
    </div>
   <!-- Footer Section Begin -->
   <footer class="footer-section" style="margin-top: 50px;">
        <div class="container">
            <div class="footer-text">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ft-about">
                            <p>We inspire and reach millions of travelers<br /> across 90 local websites</p>
                            <div class="fa-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-tripadvisor"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="ft-contact">
                            <h6>Contact Us</h6>
                            <ul>
                                <li>(12) 345 67890</li>
                                <li>info.colorlib@gmail.com</li>
                                <li>856 Cordia Extension Apt. 356, Lake, United State</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="ft-newslatter">
                            <h6>New latest</h6>
                            <p>Get the latest updates and offers.</p>
                            <form action="#" class="fn-form">
                                <input type="text" placeholder="Email">
                                <button type="submit"><i class="fa fa-send"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <ul>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Terms of use</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Environmental Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <div class="co-text">
                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="Resources/User/js/jquery-3.3.1.min.js"></script>
    <script src="Resources/User/js/bootstrap.min.js"></script>
    <script src="Resources/User/js/jquery.magnific-popup.min.js"></script>
    <script src="Resources/User/js/jquery.nice-select.min.js"></script>
    <script src="Resources/User/js/jquery-ui.min.js"></script>
    <script src="Resources/User/js/jquery.slicknav.js"></script>
    <script src="Resources/User/js/owl.carousel.min.js"></script>
    <script src="Resources/User/js/main.js"></script>
</body>
</html>