<?php
include('connection.php');
session_start();
/*********************    Select Users    ******************/
function sel_users($conn)
{
    $stmt = "SELECT * FROM users ";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/*****************         SESSION Info       *****************/
function session_info($id, $name, $username, $email, $pass , $role)
{
    $_SESSION['user_id'] = $id;
    $_SESSION['name'] = $name;
    $_SESSION['username'] = $username;
    $_SESSION['email'] = $email;
    $_SESSION['password'] = $pass;
    $_SESSION['role'] = $role;
    if ($role== 1) {
        header('location:./Templates/Books.php');
    }
    else{header("location:./Templates/home.php");}
}
/*****************            Login           ************************* */
function login($conn)
{
    $result = sel_users($conn);
    foreach ($result as $value) {
        if ($_POST['username'] == $value['username'] && $_POST['password'] == $value['password']) {
                session_info($value['id'], $value['name'], $_POST['username'], $value['email'], $_POST['password'],$value['role']);

        } else {
            header('..../signin.php');
        }
    }
}
/******************            SignUp           ************************* */
function signup($conn, $name, $username, $email, $pass)
{
    $stmt = "INSERT INTO users (name, username,email, password) VALUES ('$name','$username','$email','$pass')";
    $conn->exec($stmt);
    $id = $conn->lastInsertId();
    $result = $conn->query("SELECT id ,name , username , email , password FROM users WHERE id =$id");
    $result = $result->fetchAll();
    $result = $result[0];
?>
    <script>
        alert("Register successful")
    </script>
    <?php
    session_info($result['id'], $result['name'], $result['username'], $result['email'], $result['password'],0);
}
/*********************        CATEGORY      **************** */
function sel_category($conn)
{
    $stmt = "SELECT * FROM categories";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/*********************        AUTHOR     **************** */
function sel_author($conn)
{
    $stmt = "SELECT * FROM authors";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/*********************        SELECT Books         *********************** */
function BooksAll($conn)
{
    $stmt = "SELECT books.id,books.title , c_name , a_name, price , quantity, image from books , categories , authors Where categories.id=books.category_id  And authors.id=books.author_id";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/** Select By Category & Author */
function BooksCat_Aut($category, $author, $conn)
{
    $stmt = "SELECT books.id,title , c_name , a_name, price , quantity , image from books , categories , authors where books.category_id=$category and books.author_id=$author and categories.id=$category  And authors.id=$author";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/** Select By Category */
function BooksCat($category, $conn)
{
    $stmt = "SELECT books.id,title , c_name , a_name, price , quantity , image from books , categories , authors where categories.id=$category and categories.id=books.category_id and authors.id=books.author_id";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/** Select By Author */
function BooksAut($author, $conn)
{
    $stmt = "SELECT books.id,title , c_name , a_name, price , quantity , image from books , categories , authors where authors.id=$author and categories.id=books.category_id and authors.id=books.author_id";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result;
}
/** Select By id */
function sel_book($id, $conn)
{
    $stmt = "SELECT books.id,books.title , c_name , a_name, price , quantity , image from books , categories , authors Where books.id=$id AND categories.id=books.category_id  And authors.id=books.author_id";
    $result = $conn->query($stmt);
    $result = $result->fetchAll();
    return $result[0];
}
/******************               Select cart           **************************/
function sel_cart($conn)
{
    $result = $conn->query("SELECT book_id FROM cart where user_id=$_SESSION[user_id]");
    $result = $result->fetchAll();
    return $result;
}

/*******************           Select carts                *************************/
function sel_carts($conn)
{
    $result = $conn->query("SELECT books.id,title , c_name , a_name , price , quantity , image FROM books, categories , authors , cart where user_id=$_SESSION[user_id] AND books.id=book_id AND books.category_id=categories.id AND books.author_id=authors.id");
    $result = $result->fetchAll();
    return $result;
}

/*****************            Select Reservations    ****************************/
function Select_Reservations($conn){
    $stmt="SELECT bill.id from bill where  bill.user_id = $_SESSION[user_id]";
     if(!empty($stmt)){
        $view=$conn->query($stmt);
        $view=$view->fetchAll();
        return $view;
    }
    else {
             return null;
     }

}

/*********************        Add book to Cart       *********************** */

function AddCart($id, $conn)
{
    $conn->exec("INSERT INTO cart Values($id,$_SESSION[user_id])");
    echo "<script>window.top.location='../Templates/home.php'</script>";
}

/*********************        Add Cart validation      *********************** */
function cart_val($id, $conn)
{
    $result = sel_cart($conn);
    $flag = 0;
    foreach ($result as $val) {
        if ($val['book_id'] == $id) {
            $flag += 1;
        }
    }
    if ($flag !== 0) {
    ?>
        <script>
            alert('It has been Carted before')
        </script>
<?php
        echo "<script>window.top.location='../Templates/home.php'</script>";
    } else {
        AddCart($id, $conn);
    }
}

/*****************             Add Reservation    ****************************/
function add_res($id, $q, $conn)
{
    $conn->exec("INSERT INTO bill (user_id) VALUES ($_SESSION[user_id])");
    $bill_id = $conn->lastInsertId();
    foreach ($id as $key => $val) {
        $quantity = sel_book($val, $conn);
        $quantity = $quantity['quantity'];
        $quantity = $quantity - $q[$key];
        //INSERT INTO `bill_contents` (`bill_id`, `book_id`, `quantity`) VALUES ('15', '31', '1');
        $conn->exec("INSERT INTO bill_contents (bill_id , book_id , quantity) VALUES ($bill_id, $val, $q[$key])");
        $conn->exec("UPDATE books SET quantity=$quantity WHERE id=$val");
        DeleteCart($val, $conn);
    }
}
/**************************       Select bill           ************************ */
function selectBill($conn,$id){
    $stmt="SELECT * from bill where id = $id";
    $result=$conn->query($stmt);
    $result=$result->fetchAll();
    return $result[0] ;
}

/******************               Delete Cart           **************************/
function DeleteCart($b_id, $conn)
{
    $conn->exec("DELETE FROM cart where book_id=$b_id AND user_id=$_SESSION[user_id]");
    echo "<script>window.top.location='../Templates/cart.php'</script>";
}
/******************               Delete Cart           **************************/
function del_bill($bill_id,$conn)
{
    $books_id = $conn->query("SELECT books.id , bill_contents.quantity  From bill_contents , books WHERE bill_id=$bill_id AND bill_contents.book_id = books.id");
    foreach ($books_id as $val) {
        $q = sel_book($val['id'], $conn);
        $q = $q['quantity'];
        $q += $val['quantity'];
        $conn->exec("UPDATE books SET quantity=$q WHERE id=$val[id]");
    }
    $conn->exec("DELETE FROM bill_contents WHERE bill_id=$bill_id");
    $conn->exec("DELETE FROM bill WHERE id=$bill_id");
    echo "<script>window.top.location='../Templates/reservation.php'</script>";
}
?>