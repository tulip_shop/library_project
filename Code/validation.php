<?php
include("connection.php");
include("UserFunctions.php");
$passErr=$userErr=$nameErr=$emailErr="";
$Flag=0;
if(isset($_POST['login'])){
 login_val($conn,$userErr,$passErr);
}
if(isset($_POST['signup'])){
signup_val($conn,$nameErr,$userErr,$emailErr,$passErr);
}
/****************         login validation       *********** */
function login_val($conn, &$userErr , &$passErr){
    if(empty($_POST['username']))
    {
        $userErr="the user-name is required ";
    }
    else{
        $username=filter_var($_POST['username'],FILTER_SANITIZE_STRING);
        if($username == ""){
            $userErr="invalid user-name";
        }
    }

 //*************************************************** */
 if(empty($_POST['password'])){
     $passErr="the password is required";
 }
 else {
    $passCh=filter_var($_POST['password'],FILTER_SANITIZE_STRING);
    if($passCh == ""){
        $passErr="invalid password";
    }
 }
 //****************************************************** */

 if($userErr =="" && $passErr == ""){
    login($conn);
 }
}
/****************         Signup validation       *********** */
function signup_val($conn,&$nameErr,&$userErr,&$emailErr,&$passErr){
    if(empty($_POST['name'])){
        $nameErr="the name is required ";
    }
    else{
        $name=filter_var($_POST['name'],FILTER_SANITIZE_STRING);
        if($name == "" || !preg_match("/^[a-zA-Z ]*$/",$name)){
            $nameErr="invalid name";
        }
    }

 //*************************************************** */
    if(empty($_POST['username']))
    {
        $userErr="the user-name is required ";
    }
    else{
        $username=filter_var($_POST['username'],FILTER_SANITIZE_STRING);
        if($username == ""){
            $userErr="invalid user-name";
        }
    }
 //*************************************************** */
 if(empty($_POST['email']))
 {
     $emailErr="the email is required ";
 }
 else{
     $emailname=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
     if($emailname == ""){
         $emailErr="invalid email";
     }
 }
 //*************************************************** */
 if(empty($_POST['password'])){
     $passErr="the password is required";
 }
 else {
    $passCh=filter_var($_POST['password'],FILTER_SANITIZE_STRING);
    if($passCh == ""){
        $passErr="invalid password";
    }
 }
 //****************************************************** */

 if($nameErr == ""&& $userErr =="" && $emailErr == "" && $passErr == ""){
    $result = sel_users($conn);
    foreach($result as $val){
        if($_POST['username'] == $val['username'] || $_POST['email'] == $val['email']){
            if($_POST['username'] == $val['username'] && $_POST['email'] == $val['email']){
                 $userErr ="Invalid: Entered user name already used ";
                 $emailErr ="Invalid: Entered user name already used ";
            }
            else if($_POST['username'] == $val['username']){
                $userErr ="Invalid: Entered user name already used ";
           }
           else {$emailErr ="Invalid: Entered user name already used ";}
        }
    }
    if($userErr =="" && $emailErr == ""){
        signup($conn,$_POST['name'],$_POST['username'],$_POST['email'],$_POST['password']);
    }
 }
}
?>