<?php
include("connection.php");
include("delete.php");
/******************************************************************************************************************** */
/*********************************************Authors functions****************************************************** */

/***********************************************Fetch All*************************************************************** */
function SelectAllAuthors($conn){
    $stmt = "SELECT * from authors" ;
    $result=$conn->query($stmt);
    $result=$result->fetchAll();
    return $result ;
}
/**************************************************Add Author************************************************************ */
function AddAuthor($conn ,$name){
    if(!empty($name))
    {
       $stmt="INSERT INTO authors (a_name) values('$name')";
       $conn->exec($stmt);
       echo "<script>window.top.location='../Templates/Authors.php'</script>";
    }
    else
    {
        echo "<script>window.top.location='../Templates/AddAuthor.php'</script>";
    }
}
/************************************************Select 1 Author************************************************************** */
function SelectAuthor($id,$conn)
{
   $stmt="SELECT * from authors where id=$id";
    $result=$conn->query($stmt);
    $result=$result->fetchAll();

    return $result[0];
}

/************************************************Update Author************************************************************** */
function update($id,$conn ,$name){
    if(!empty($name))
    {
    $stmt="UPDATE authors set a_name ='$name' where id=$id ";
    $conn->exec($stmt);
    echo "<script>window.top.location='../Templates/Authors.php'</script>";
    }
    else{
        echo "<script>window.top.location='../Templates/editAuthor.php'</script>";
    }
}
/******************************************************************************************************************** */
/*********************************************Books functions******************************************************** */
/***********************************************Fetch All*************************************************************** */
function SelectBooks($conn){
    $stmt="SELECT books.id, title, price, quantity, image, a_name, c_name from books, authors, categories
    where books.category_id=categories.id AND authors.id=books.author_id ";
    $view=$conn->query($stmt);
    $view=$view->fetchAll();
    return $view;
}
/***********************************************Select book*************************************************************** */
function AddBook($title, $category_id,$author_id,$price,$quantity,$img,$conn){
    $stmt3="INSERT INTO books (title,category_id,author_id,price,quantity,image)
    VALUES ('$title', $category_id,$author_id,$price,$quantity,'$img')";
    $conn->exec($stmt3);
    header("location:../Templates/Books.php");
}
/***********************************************Select book*************************************************************** */
function SelectBook($id,$conn){
    $stmt="SELECT * from books where id=$id";
    $result=$conn->query($stmt);
    $result=$result->fetchAll();
    return $result;
}
/***********************************************update book*************************************************************** */
function  updateBook($id,$conn,$name,$p,$quantit,$cat,$img ,$a){

    $stmt="UPDATE books set title ='$name', quantity=$quantit, category_id=$cat ,price=$p ,image='$img',author_id=$a where id=$id ";
    $conn->exec($stmt);
    echo "<script>window.top.location='../Templates/Books.php'</script>";
}

/******************************************************************************************************************** */
/*********************************************Categories functions******************************************************** */
/***********************************************Fetch All*************************************************************** */
function SelectAllCat($conn){
    $stmt="SELECT * from categories ";
    $view=$conn->query($stmt);
    $view=$view->fetchAll();
    return $view;
}
/***********************************************Add category*************************************************************** */
function AddCategory($conn,$name){
    if(!empty($name))
    {
       $stmt="INSERT INTO categories (c_name) values('$name')";
       $conn->exec($stmt);
    echo "<script>window.top.location='../Templates/Category.php'</script>";

    }
    else
    {
        echo "<script>window.top.location='../Templates/AddCategory.php'</script>";
    }
}
/***********************************************Select category*************************************************************** */
function SelectCat($id,$conn){
    $stmt="SELECT * from categories where id=$id";
    $result=$conn->query($stmt);
    $result=$result->fetchAll();
    return $result[0];
}
/***********************************************update category*************************************************************** */
// function updateCat($id,$conn ,$CatName){
//     if(!empty($CatName))
//     {
//     $stmt="UPDATE categories set c_name ='$CatName' where id=$id ";
//     $conn->exec($stmt);
//     echo "<script>window.top.location='../Templates/Category.php'</script>";
//     }
//     else{
//     echo "<script>window.top.location='../Templates/editCat.php'</script>";
//     }
// }
/******************************************************************************************************************** */
/*********************************************Bills functions******************************************************** */
/***********************************************Fetch All*************************************************************** */
function SelectAllBills($conn){
    $stmt="SELECT bill.id,status, name from bill , users where  bill.user_id = users.id";
    $view=$conn->query($stmt);
    $view=$view->fetchAll();
    return $view;
}
/***************************************************************************************************** */
function SelectBillCont($conn,$id){
$stmt="SELECT bill_id, image, book_id, title, bill.timestamp, bill_contents.quantity, price, bill.user_id FROM bill_contents, books, bill where bill_id = $id AND books.id = bill_contents.book_id AND bill.id=$id";
//$stmt="SELECT * from bill_contents where bill_id = $id";
$view=$conn->query($stmt);
    $view=$view->fetchAll();
    return $view;
}

function  FilterBills($conn,$id){
$stmt="SELECT bill.id, name , status from bill , users where  bill.user_id = users.id AND bill.id=$id";
$view=$conn->query($stmt);
$view=$view->fetchAll();
return $view;

}

function Filterdate($conn,$date){
    $stmt="SELECT bill.id, name ,bill.status from bill , users where  bill.user_id = users.id AND bill.timestamp LIKE  '$date%%%' ";
    $view=$conn->query($stmt);
    $view=$view->fetchAll();
    return $view;
}

function BookTotal($quan,$price,$b_id){
return $total= (int)$quan * (int)$price;
}

function Total($conn,$id){
    $Total=0;
    $Books=SelectBillCont($conn,$id);
    foreach ($Books as  $value) {
        $Total+=BookTotal($value['quantity'],$value['price'],$value['book_id']); 
    }
    if (empty($Total)){
        deleteBill($id,$conn);
    }
return $Total;
}


function FormatDate($month,$day,$year){
    if($month<10){
        $month="0".$GLOBALS['month'];
    }
   else {
    $month=$GLOBALS['month'];
     }
   if($day<10){
    $day="0".$GLOBALS['day'];
    }
    else {
    $day=$GLOBALS['day'];
    }
    return $date=$year."-".$month."-".$day;
}

?>