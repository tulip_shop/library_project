<?php

include("AdminFunctions.php");
include("connection.php");
 /***************************************Validate the input************************************* */
 $err="";
 if(isset($_POST['send'])){

     if(empty($_POST['bookName']))
     {
         $err="this field is required";
     }
     else{
         $name=filter_var($_POST['bookName'],FILTER_SANITIZE_STRING);
         if($name == ""){
             $err="invalid input";
         }
     }
     if($_POST['price']=='')
     {
         $err="this field is required";
     }
     else{
         $name=filter_var($_POST['price'],FILTER_VALIDATE_FLOAT);
         if($name == ""){
             $err="invalid input";
         }
     }
     if($_POST['quantity']=='')
     {
         $err="this field is required";
     }
     else{
         $name=filter_var($_POST['price'],FILTER_VALIDATE_FLOAT);
         if($_POST['quantity']<=0 || $name =="")
         {
             $err="invalid input";
         }
     }
 /*************************************Validate image******************************************************** */
      if (empty($_FILES['img']['tmp_name']))
     {
         $_msg = "please upload the img";
     }
   else
     {
         $fnm1=$_FILES['img']['name'];
         $enfile1 = md5(time());
         $dest1="../Resources/images/".$fnm1;
         move_uploaded_file($_FILES['img']['tmp_name'],$dest1);
     }
/*************************************************************************************************************** */

if($err=="")
{
    if($_POST['type']==="new")
    {
        extract($_POST);
    $title =$bookName;
    $category_id=$category;
    $author_id=$author;
    $price=$price;
    $quantity=$quantity;
    $img=$_FILES['img']['name'];
    AddBook($title, $category_id,$author_id,$price,$quantity,$img,$conn);
    }
    else
    {
  extract($_POST);
  $id=$type;
  $name=$bookName;
  $p= $price;
  $quantit=$quantity;
  $cat= $category;
  $a=$author;
  $img=$_FILES['img']['name'];
  //updateBook($GLOBALS['id'],$conn,$GLOBALS['name'],$GLOBALS['p'],$GLOBALS['quantit'],$GLOBALS['cat'],$GLOBALS['img'] ,$GLOBALS['a']);
  updateBook($id,$conn,$name,$p,$quantit,$cat,$img ,$a);

}
}
else
{
    header("location:../Templates/NewBook.php");
}
}
?>