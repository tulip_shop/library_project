<?php
include('connection.php');
//include('functions.php');
extract($_GET);

if (isset($Aid)){
    $id=$Aid;
    deleteAut($GLOBALS['id'],$conn);
}
elseif(isset($Cid)){
    $Catid=$Cid;
    deleteCat($GLOBALS['Cid'],$conn);
}
elseif(isset($Bid)){
    $Bid=$Bid;
    deleteBook($GLOBALS['Bid'],$conn);

}
elseif (isset($GLOBALS['Bill_id'])) {
 $Bill_id=$Bill_id;
 deleteBill($GLOBALS['Bill_id'],$conn);
}
function deleteAut($id,$conn)
{   $stmtCheck="SELECT author_id from books where author_id=$id";
    if(!empty($stmtCheck)){
        $Del="DELETE from books where author_id=$id";
        $conn->exec($Del);
    }
    $stmtD="DELETE from authors where id=$id";
    $conn->exec($stmtD);
    header('location:../Templates/Authors.php') ;
}

function deleteCat($Cid,$conn)
{
    $stmtCheck="SELECT books.id from books where category_id=$Cid";
    $result=$conn->query($stmtCheck);

    if(!empty($result)){

        $stmtD="DELETE from books where category_id=$Cid";
        $conn->exec($stmtD);
    }

    $stmtD="DELETE from categories where id=$Cid";
    $conn->exec($stmtD);
    echo "<script>window.top.location='../Templates/Category.php'</script>";
    header('location:../Templates/Category.php') ;
}

function  deleteBook($Bid,$conn){
    $stmtCheck="SELECT book_id from bill_contents where book_id=$Bid";
    if(!empty($stmtCheck)){
        $Del="DELETE from bill_contents where book_id=$Bid";
        $conn->exec($Del);
         $checkBill="SELECT bill_id from bill_contents";
    }
    $checkCart="SELECT book_id from cart where book_id=$Bid";
    if (!empty($checkCart)){
        $conn->exec("DELETE from cart where book_id=$Bid");
    }
    $stmtD="DELETE from books where id=$Bid";
    $conn->exec($stmtD);
    header('location:../Templates/Books.php') ;
}
function deleteBill($Bill_id,$conn){
    $books_id = $conn->query("SELECT books.id , bill_contents.quantity  From bill_contents , books WHERE bill_id=$Bill_id AND bill_contents.book_id = books.id");
    foreach ($books_id as $val) {
        $stmt = "SELECT books.id,books.title , c_name , a_name, price , quantity , image from books , categories , authors Where books.id=$val[id] AND categories.id=books.category_id  And authors.id=books.author_id";
        $result = $conn->query($stmt);
        $result = $result->fetchAll();
        $q= $result[0];
        $q = $q['quantity'];
        $q += $val['quantity'];
        $conn->exec("UPDATE books SET quantity=$q WHERE id=$val[id]");
    }
    $conn->exec("DELETE FROM bill_contents WHERE bill_id=$Bill_id");
    $conn->exec("DELETE FROM bill WHERE id=$Bill_id");
    echo "<script>window.top.location='../Templates/viewBills.php'</script>";
}
?>