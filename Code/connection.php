<?php
$dsn="mysql:host=localhost;port=3306;dbname=library";
$dbuser="root";
$dbpass="";
try{
    $conn = new PDO($dsn,$dbuser,$dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo "connection done";
}
catch(PDOException $e){
    echo "Connection failed: ". $e->getMessage();
}
?>