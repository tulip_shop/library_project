<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Library</title>
    <!-- Css Styles -->
    <link rel="stylesheet" href="../Resources/User/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/style.css" type="text/css">
</head>

<body>
    <!-- Header Section Begin -->
        <div class="menu-item">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo">
                            <a href="home.php">
                                <!-- <img src="../Resources/User/assets/img/logo.png" alt=""> -->
                                <h3>e-Book</h3>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="nav-menu">
                            <nav class="mainmenu">
                                <ul>
                                    <li class="active"><a href="home.php">Home</a></li>
                                    <li><a href="reservation.php">My Reservation</a></li>
                                    <li><a href="account.php">Account</a></li>
                                    <li><a href="../index.php">Sign out</a></li>
                                    <li><a href="cart.php" class="icon_cart_alt"></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Header End -->
</body>
</html>