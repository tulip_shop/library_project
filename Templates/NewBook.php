<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
</head>

<body>
    <!-- ============================================================== -->
<?php
 include("layout.php");
 include("../Code/connection.php");
 include("../Code/validationBook.php");
    $categories=SelectAllCat($conn);
    $authors=SelectAllAuthors($conn);
    ?>

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
    
            <div class="container-fluid">
                <div class="col-6 offset-9">
                <li class="list-inline class="col-6 offset-9""> 
                <a  class="btn btn-cyan col-6 text-white" href="Books.php">View books</a>
                </li>
                </div>
<form  class="col-3 offset-4" method="POST" enctype="multipart/form-data" action="../Code/validationBook.php">
<input name="type" hidden value="new">

         Book Image :<input type="file" name="img" class=" btn btn-cyan col-12 text-white fa fa-upload " Required>
         <span><?php echo $err?></span><br>
         Book title: <input type="text" name="bookName" class=" form-control " Required>
         <span><?php echo $err?></span><br>
         Price:<input type="float" name="price" class=" form-control " Required>
         <span><?php echo $err?></span><br>
         Quantity:<input type="number" name="quantity" class=" form-control "Required>
         <span><?php echo $err?></span><br>
        
            <!-- /*****************************************Categories List******************************************** */ -->
            <select class='form-control' name='category' required>
            <option selected disabled hidden>Category</option>
            <?php foreach ($categories as  $value) {
            
               echo "<option value='$value[id]'>".$value[c_name]."</option>";
             } ?>
            </select><br>
           
            <!-- /*******************************************Authors List******************************************** */ -->
            <select class='form-control' name='author' required>
            <option selected disabled hidden>Author</option>
    <?php foreach ($authors as  $value)
            {
               echo "<option value='$value[id]'>".$value[a_name]."</option>";
            }
           echo "</select><br>"; 
         ?>
         <input type ="submit" name="send" class="btn btn-cyan offset-4 text-white"><br>

    </form>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
</body>

</html>