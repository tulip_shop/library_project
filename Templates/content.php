<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

</head>

<body>
    <?php
include("layout.php");
include("../Code/connection.php");
include("../Code/AdminFunctions.php");

$id=$_GET['id'];
$content= SelectBillCont($conn,$id);
    ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
    <div class="container-fluid">
    <div style="height:50px"></div>
        <div class="offset-2">
        <table class="table">
        <tr class="text-center">
								  <td></td>
                                <th>Title</th>
                                <th></th>
                                <th>Price</th>
                                <th></th>
                                <th>Quantity</th>
                                <th></th>
						                		<th>Total</th>

						      </tr>
                              <?php foreach($content as $value){
                                    $totalbook= BookTotal($value['quantity'],$value['price'],$value['bill_id']);
                                    $date=$value['timestamp'];
                                    $user=$value['user_id'];
                                    ?>
						        <tr class="text-center">
							    <td><img src='../Resources/images/<?php echo $value['image'] ?>' style='width:100% ;height:250px; width:150px; '
                                 class='m-auto card-img align-items-center '></td>
                                <td><?php echo $value['title'] ?><td>
                                <td><?php echo $value['price']."$" ?><td>
								<td><?php echo $value['quantity'] ?><td>
								<td><?php echo $totalbook."$" ?></td>

							  </tr>
                                <?php } ?>
                                <tr >

                                <td><h5 class="d-inline col-1  " style="font-family:sans-serif">Bill ID :<?php echo $id ?></h5></td>
                                <td><h5 class="d-inline "  style="font-family:sans-serif">Reservation date :<?php echo $date ?></h5></td>
                                <td><h5 class="d-inline "  style="font-family:sans-serif">User id :<?php echo $user ?></h5></td>
                                <td></td>
                                <td></td>

                                <td><h5 class=" col-12 offset-9"  style="font-family:sans-serif">Totla price :<?php echo Total($conn,$id).'$'; ?></h5> </td>

                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
        </table>

    </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

</body>

</html>