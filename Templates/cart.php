<?php
		include('../Code/UserFunctions.php');
		include('../Code/connection.php');
		$result = sel_carts($conn);
		if (isset($_POST['add_res'])) {
			add_res($_POST['id'],$_POST['quantity'],$conn);
		}
?>

<!DOCTYPE html>
<head>
	<title>Carts</title>
</head>

<body>
<?php include('header.php'); ?>
	<div class="container" style="margin-top:100px;">
<h2 class="breadcrumb-text">Carts</h2>
		<table class="table" style="margin-top:50px;">
			<thead class="thead-primary">
				<tr class="text-center">
					<th>Image</th>
					<th>Title</th>
					<th>Category</th>
					<th>Author</th>
					<th>Price</th>
					<th>Quantity</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<form method="POST" class="container col-6 offset-3 form-inline">
					<?php foreach ($result as $value) { ?>
						<tr class="text-center">
							<td><input type="checkbox" class="form-check-input" name="id[]" value=<?php echo $value['id']?>></td>
							<td><?php echo $value['title'] ?></td>
							<td><?php echo $value['c_name'] ?></td>
							<td><?php echo $value['a_name'] ?></td>
							<td><?php echo $value['price'] ?>$</td>
							<?php if($value['quantity']==0){ DeleteCart($value['id'], $conn);}?>
							<td><select name='quantity[]' class='offset-3'>
								<?php for($i=1 ; $i<$value['quantity']+1 ; $i++){?>
								<option value=<?php echo $i?>><?php echo $i?></option>
								<?php } ?>
							</select></td>
							<td><a href="../Code/del_cart.php?id=<?php echo $value['id'];?>" class="icon_close"></a></td>
						</tr>
					<?php } ?>
			</tbody>
		</table><br>
		<input type="submit" name="add_res" class="btn btn-info col-lg-4 offset-lg-4" style="border-color:#dfa974; background-color:#dfa974" value="Reservation">
			</form>
	</div>
	<?php include('footer.php'); ?>
</body>

</html>