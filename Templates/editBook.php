<!DOCTYPE html>
<html lang="en">
<body>
<?php
include("../Code/connection.php");
include("../Code/AdminFunctions.php");
include("layout.php");

$categories=SelectAllCat($conn);
$authors=SelectAllAuthors($conn);


    $id=$_GET['id'];
    $result=SelectBook($id,$conn);
  $err="";

?>

 <div class="page-wrapper">
    <div class="container-fluid">
<div class="col-6 offset-9">
<a  class="btn btn-block btn-cyan col-6  text-white" href="Books.php" >View Books</a><br><br>
</div>
<form  class="col-3 offset-4" method="POST" enctype="multipart/form-data" action="../Code/validationBook.php">

    Book Image :<input type="file" name="img" class=" btn btn-cyan col-12 text-white fa fa-upload"  Required>
    <span><?php echo $err?></span><br>

        <input name="type" hidden value="<?php echo $result[0]['id']; ?>" >
         Book title: <input type="text" name="bookName" class=" form-control "value="<?php echo $result[0]['title']; ?>" Required>
         <span><?php echo $err?></span><br>
         Price:<input type="float" name="price" class=" form-control "value="<?php echo $result[0]['price'] ?>" Required>
         <span><?php echo $err?></span><br>
         Quantity:<input type="number" name="quantity" class=" form-control " value="<?php echo $result[0]['quantity'] ?>" Required>
         <span><?php echo $err?></span><br>
         <?php
            /*****************************************Categories List******************************************** */
            echo "
            <select class='form-control' name='category' required >
            <option selected disabled hidden>Category</option>";
            foreach ($categories as  $value)
             {
                echo "<option value='$value[id]'>".$value['c_name']."</option>";
             }
            echo "</select><br>";
            /*******************************************Authors List******************************************** */
            echo "<select class='form-control' name='author' required>
            <option selected disabled hidden>Author</option>";
            foreach ($authors as  $value)
            {
               echo "<option value='$value[id]'>".$value['a_name']."</option>";
            }
           echo "</select><br>";
         ?>
         <input type ="submit" name="send" class="btn btn-cyan offset-4 text-white"><br>

    </form>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->


</body>

</html>