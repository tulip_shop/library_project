<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Books</title>

</head>

<body>
<?php
include("layout.php");
include("../Code/connection.php");
include("../Code/AdminFunctions.php");
if (isset($_POST['1']))
{
    header('location:NewBook.php');
}
$view = SelectBooks($conn);
    ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">

    <div class="container-fluid">
    <!-- <div style="height:50px"></div> -->
        <ul class="col-6 offset-9">
            <li class="list-inline"> <a  class="btn btn-block btn-cyan col-6 text-white" href="NewBook.php" >Add new book</a></li>
        </ul>
        <div class="offset-3">
        <table class="table ">
              <tr>
              <th>Image</th>
              <th>Title</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Author</th>
              <th>Category</th>
              <th> </th>

              </tr>
<?php
           foreach ($view as $value)
           { ?>

              <tr>
              <td><img src='../Resources/images/<?php echo $value['image']?>' style='width:100% ;height:200px; width:150px; '></td>
              <td><?php echo $value['title']?></td>
              <td><?php echo $value['price'] ?></td>
              <td><?php echo $value['quantity'] ?></td>
              <td><?php echo $value['a_name'] ?></td>
              <td><?php echo $value['c_name'] ?></td>
              <td>
              <a class='fa fa-edit text-cyan' href='editBook.php?id=<?php echo $value['id'] ?>'></a>
              <a class='fa fa-trash text-cyan' href='../Code/delete.php?Bid=<?php echo $value['id'] ?>'></a>
              </td>
              </tr>

          <?php } ?>
          </table>

</div>
    </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

</body>

</html>