<?php
//include('header.php');
//include('../Code/UserFunctions.php');
include("../Code/AdminFunctions.php");

$id=$_GET['id'];
$content= SelectBillCont($conn,$id);
?>
<!DOCTYPE html>
<body>
		<div class="container" style="margin-top: 100px;">
			<h2 class="breadcrumb-text">My Bill</h2>
	    				<table class="table" style="margin-top: 50px;">
						    <thead class="thead-primary">
						      <tr class="text-center">
								  <td></td>
                                <th>Title</th>
                                <th></th>
                                <th>Price</th>
                                <th></th>
                                <th>Quantity</th>
                                <th></th>
						                		<th>Total</th>

						      </tr>
						    </thead>
						    <tbody>
								<?php foreach($content as $value){
                                    $totalbook= BookTotal($value['quantity'],$value['price'],$value['bill_id']);
                                    $date=$value['timestamp'];
                                    $user=$value['user_id'];
                                    ?>
						        <tr class="text-center">
							    <td><img src='../Resources/images/<?php echo $value['image'] ?>' style='width:100% ;height:250px; width:150px; '
                                 class='m-auto card-img align-items-center '></td>
                                <td><?php echo $value['title'] ?><td>
                                <td><?php echo $value['price']."$" ?><td>
								<td><?php echo $value['quantity'] ?><td>
								<td><?php echo $totalbook."$" ?></td>

							  </tr>
                                <?php } ?>

                                <tr >

                                <td><h5 class="d-inline col-1 offset-2 " style="font-family:sans-serif">Bill ID :<?php echo $id ?></h5></td>
                                <td><h5 class="d-inline offset-1"  style="font-family:sans-serif">Reservation date :<?php echo $date ?></h5></td>
                                <td><h5 class="d-inline offset-1"  style="font-family:sans-serif">User id :<?php echo $user ?></h5></td>
                                <td></td>
                                <td></td>

                                <td><h5 class=" col-12 offset-9"  style="font-family:sans-serif">Totla price :<?php echo Total($conn,$id).'$'; ?></h5> </td>

                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td><button onclick="window.print()" class="btn btn-info float-right">Print</button></td></tr>
                                </tr>
                            </tbody>

						  </table>
		</div>
		<div style="height:50px;"></div>
<?php include('footer.php');?>
</body>
</html>