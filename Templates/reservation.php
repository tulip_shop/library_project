<?php
include('header.php');
include('../Code/UserFunctions.php');
include("../Code/AdminFunctions.php");
$result = Select_Reservations($conn);
?>
<!DOCTYPE html>
<body>
		<div class="container" style="margin-top: 100px;">
			<h2 class="breadcrumb-text">My Reservation</h2>
	    				<table class="table" style="margin-top: 50px;">
						    <thead class="thead-primary">
						      <tr class="text-center">
								  <td></td>
								<th>Bill ID</th>
								<th></th>
								<th>Total</th>
								<td></td>
						      </tr>
						    </thead>
						    <tbody>
								<?php foreach($result as $val){?>
						        <tr class="text-center">
							    <td><a href="../Code/del_reservation.php?billid=<?php echo $val['id'];?>" class="icon_close"></a></td>
								<td><?php echo $val['id']?><td>
								<td><?php echo Total($conn,$val['id']).'$';?></td>
								<td><a class="btn btn-info" href="Billcontent.php?id=<?php echo $val['id']?>">Bill details</a></td>
							  </tr>
								<?php } ?>
						    </tbody>
						  </table>
		</div>
		<div style="height:50px;"></div>
<?php include('footer.php');?>
</body>
</html>