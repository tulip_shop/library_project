<?php include('header.php');
include('../Code/connection.php');
include('../Code/UserFunctions.php');
extract($_POST);
$c_result = sel_category($conn);
$a_result = sel_author($conn);
$result = BooksAll($conn);
if (isset($search)) {
    if ($category !== "" || $author !== "") {
        if ($category !== "" && $author !== "") {
            $result = BooksCat_Aut($category, $author, $conn);
        } elseif ($category !== "" && $author == "") {
            $result = BooksCat($category, $conn, $result);
        } else {
            $result =  BooksAut($author, $conn, $result);
        }
    }
}
?>
<!DOCTYPE html>
<body>
    <section class="hero-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="hero-text" >
                        <h1>A cover is not THE BOOK so open it up & take a look</h1>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5 offset-xl-2 offset-lg-1">
                    <div class="booking-form">
                        <h3>Search Book</h3>
                        <form method="post">
                            <div class="select-option">
                                <label for="cate">Category : </label>
                                <select id="cate" name="category">
                                    <option value="" hidden></option>
                                    <?php foreach ($c_result as $val) { ?>
                                        <option value="<?php echo $val['id']; ?>"><?php echo $val['c_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select-option" name="author">
                                <label for="author">Author : </label>
                                <select id="author" name="author">
                                    <option value="" hidden></option>
                                    <?php foreach ($a_result as $val) { ?>
                                        <option value="<?php echo $val['id']; ?>"><?php echo $val['a_name'] ?></option>
                                    <?php } ?>
                                </select>
                                    </div>
                            <input type="submit" name="search" value="Search" class="btn btn-default col-lg-12" style="border-color:#dfa974; color:#dfa974;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-slider owl-carousel">
            <div class="hs-item set-bg" data-setbg="../Resources/User/assets/img/li.jpg"></div>
            <div class="hs-item set-bg" data-setbg="../Resources/User/assets/img/lib1.jpg"></div>
            <div class="hs-item set-bg" data-setbg="../Resources/User/assets/img/lib2.jpg"></div>
            <div class="hs-item set-bg" data-setbg="../Resources/User/assets/img/books.jpg"></div>
        </div>
    </section>

    <div class="hp-room-items" style="margin-top : 50px;">
        <div class="row">
        <?php foreach ($result as $value) {?>
            <div class="col-lg-3 col-md-6 " style="margin-buttom:10px;">
                <div class="hp-room-item set-bg" data-setbg="../Resources/images/<?php echo $value['image']?>" ">

                    <div class="hr-text" style="background: rgba(16, 13, 27, 0.9); padding : 20px;">
                        <h3><?php echo $value['title']; ?></h3>
                        <h2><?php echo $value['price'];?>$</h2>
                        <table>
                            <tbody>
                                <tr>
                                    <td class="r-o">Category:</td>
                                    <td><?php echo $value['c_name']; ?></td>
                                </tr>
                                <tr>
                                    <td class="r-o">Author:</td>
                                    <td><?php echo $value['a_name']; ?></td>
                                </tr>
                                <tr>
                                    <td class="r-o">Quantity:</td>
                                    <td><?php echo $value['quantity']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="../Code/addcart.php?id=<?php echo $value['id'];?>" class="primary-btn" onclick="confirm('added to cart successfuly !')">Add Cart</a>
                    </div>
                </div>
        </div><?php } ?>
        </div>
    </div>

    <?php include('footer.php'); ?>
</body>
</html>