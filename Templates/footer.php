<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <!-- Css Styles -->
    <link rel="stylesheet" href="../Resources/User/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/flaticon.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../Resources/User/css/style.css" type="text/css">
</head>

<body>
    <!-- Footer Section Begin -->
    <footer class="footer-section" style="margin-top: 50px;">
        <div class="container">
            <div class="footer-text">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="ft-about">
                            <p>We inspire and reach millions of travelers<br /> across 90 local websites</p>
                            <div class="fa-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-tripadvisor"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="ft-contact">
                            <h6>Contact Us</h6>
                            <ul>
                                <li>(12) 345 67890</li>
                                <li>info.colorlib@gmail.com</li>
                                <li>856 Cordia Extension Apt. 356, Lake, United State</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="ft-newslatter">
                            <h6>New latest</h6>
                            <p>Get the latest updates and offers.</p>
                            <form action="#" class="fn-form">
                                <input type="text" placeholder="Email">
                                <button type="submit"><i class="fa fa-send"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <ul>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Terms of use</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Environmental Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <div class="co-text">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="../Resources/User/js/jquery-3.3.1.min.js"></script>
    <script src="../Resources/User/js/bootstrap.min.js"></script>
    <script src="../Resources/User/js/jquery.magnific-popup.min.js"></script>
    <script src="../Resources/User/js/jquery.nice-select.min.js"></script>
    <script src="../Resources/User/js/jquery-ui.min.js"></script>
    <script src="../Resources/User/js/jquery.slicknav.js"></script>
    <script src="../Resources/User/js/owl.carousel.min.js"></script>
    <script src="../Resources/User/js/main.js"></script>
</body>
</html>