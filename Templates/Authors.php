<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="BS/css/bootstrap.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Authors</title>
</head>
<body>
<?php
include("../Code/connection.php");
include("layout.php");
include("../Code/AdminFunctions.php");
$authors = SelectAllAuthors($conn);
?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">

            <div class="container-fluid">
<div class="col-6 offset-9">
<a  class="btn btn-block btn-cyan col-6  text-white" href="AddAuthor.php" >Add new Author</a><br><br>
</div>
                <table class='table col-2 offset-2'>
                    <tr>
                        <th>Authors</th>
                        <td></td>
                        <td></td>
                    </tr>
                        <?php foreach ($authors as $value) {?>
                           <tr>
                            <td><?php echo $value['a_name']?></td>
                            <td><a href='editAuthor.php?id=<?php echo $value['id']?>' class='fa fa-edit text-cyan offset-4 ' alt='Edit'></a>
                            <a href='../Code/delete.php?Aid=<?php echo $value['id'] ?>' class='fa fa-trash text-cyan offset-2' alt='Delete'></a></<a>
                            </tr>";
                      <?php  } ?>
                </table>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

</body>
</html>