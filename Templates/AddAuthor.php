<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="BS/css/bootstrap.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
include("../Code/connection.php");
include("layout.php");
include("../Code/AdminFunctions.php");

if(isset($_POST['send'])){
    $name=$_POST['authorName'];
    AddAuthor($conn,$name);
}

?>

 <div class="page-wrapper">
    <div class="container-fluid">
<div class="col-6 offset-9">
<a  class="btn btn-block btn-cyan col-6  text-white" href="Authors.php" >View Authors</a><br><br>
</div>
<form  class="col-3 offset-5" method="POST">

         Author name: <input type="text" name="authorName" class=" form-control " Required><br>
         <input type ="submit" name="send" class="btn btn-cyan text-white offset-4" value="send"> <br>
    </form>
</div>
</div>

</body>

</html>