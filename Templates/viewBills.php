<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="BS/css/bootstrap.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bills</title>
</head>
<body>
<?php
include("layout.php");
include("../Code/connection.php");
include("../Code/AdminFunctions.php");

$Bills = SelectAllBills($conn);

if(isset($_POST['filter'])){
    if (!empty($_POST['bill_id'])){
        $Bills = FilterBills($conn,$_POST['bill_id']);
    }
    else{
        extract($_POST);
        $date=FormatDate($month,$day,$year);
        $Bills=Filterdate($conn,$date); }
}
?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <div class="container-fluid">
            <form method="POST">
            <input type=number class='form-control col-1 offset-3 d-inline' placeholder="Bill ID" name="bill_id">
            <?php
            $years=2020;
            echo "
            <select class='form-control col-1 offset-2 d-inline' name='year'>
            <option selected hidden>Year</option>";
            for($i=$years;$i<2050;$i++)
            {
                echo "<option>$i</option>";
            }
            echo" </select>
            <select class='form-control col-1  d-inline' name='month'> 
            <option selected hidden>Month</option>";
            for($i=1;$i<13;$i++)
            {
                echo "<option>$i</option>";
            }
            echo" </select>
            <select class='form-control col-1  d-inline' name='day'> 
            <option selected hidden>Day</option>";
            for($i=01;$i<32;$i++)
            {
                echo "<option>$i</option>";
            }
            echo" </select> ";
?>
   <input type=submit class='form-control  fa-filter col-1 offset-1 d-inline' value="Filter" name="filter"><br><br>

<!-- ******************************Bills*********************************** -->
                <table class='table col-3 offset-2'>
                    <tr>
                        <th>Bills</th>
                        <th>User name</th>
                        <th>Total</th>
                        <th>Status</th>
                        <td></td>
                    </tr>
                    <form method="POST" action="Confirm.php">
                    <?php foreach ($Bills as $value)
                    { ?>
                        <tr>
                            <input hidden name="id" value="<?php echo $value['id'] ?>" >
                            <td><?php echo $value['id']?></td>
                            <td><?php echo $value['name']?></td>
                            <td><?php echo Total($conn,$value['id'])?></td>
                            <td><?php 
                            if ($value['status']==1){
                                echo "<a class='fa fa-check text-success' href='#'></a>";
                            } 
                            else {
                                echo "<a class='fa fa-clock text-orange ' alt='not confirmed' href='#'></a>";
                            }
                            ?></td>
                            <td>
                            <a class='fa fa-box-open  text-cyan' href='content.php?id=<?php echo $value['id']?>'></a>  
                         </a>
                            <a class='fa fa-times   text-danger' href='../Code/delete.php?Bill_id=<?php echo $value['id']?>'></a>
                            </td>
                             <td>
                             <a class='btn btn-block btn-cyan text-white col-3 d-inline'
                            href='../Code/Confirm.php?bill_id=<?php echo $value['id']?>'>confirm</a> 

                            </td>
                            </tr>
                    <?php } ?>
                    </form>
                </table>
            </div>
        </div>
        </form> 
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

</body>
</html>