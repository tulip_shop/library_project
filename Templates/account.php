<?php include('header.php');
include('../Code/UserFunctions.php');
if(isset($_POST['logout'])){
    session_destroy();
 header('location:../signin.php');
}
?>
<!DOCTYPE html>
<body>
        <div class="container" style="margin-top : 50px; margin-bottom : 50px;">
                    <div class="breadcrumb-text col-lg-12">
                        <h2>Account Info</h2>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-4">
                    <form method="post"  class="form-group">
                                Name : <input type="text" name="name" value='<?php echo $_SESSION['name']?>' class="form-control" placeholder="Name" disabled><br>
                                User Name : <input type="text" name="username" value='<?php echo $_SESSION['username']?>'  class="form-control" placeholder="User Name" disabled><br>
                                Email : <input type="email" value='<?php echo $_SESSION['email']?>'  class="form-control" placeholder="Email" disabled><br>
                                Password : <input type="text" name="password" value='<?php echo $_SESSION['password']?>'  class="form-control" placeholder="Password" disabled><br><br>
                            <!-- <input type="submit" name="update" value="Save" class="btn btn-default col-lg-4 offset-lg-2 d-inline" style="color:#dfa974; border-color:#dfa974;"> -->
                            <input type="submit" name="logout" value="Log out" class="btn btn-default col-lg-4 offset-lg-4" style="color:#dfa974; border-color:#dfa974;">
                    </form>
            </div>
    <?php include('footer.php');?>
</body>

</html>